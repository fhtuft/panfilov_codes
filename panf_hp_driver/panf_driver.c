#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <stdlib.h> //exit

#include <cuda.h> //driver api
#include <builtin_types.h>


#include "panf3d_config.h"

#define DRIVER_ERROR(ERR)  _driver_error((ERR),__FUNCTION__,__LINE__)
static void inline _driver_error(CUresult err,const char* ffname,const int line_nr) {
    if(err != CUDA_SUCCESS) {
        const char *err_string;     
        cuGetErrorString((err),&err_string); 
        fprintf(stderr,"panf cuda driver error in %s %d: %s \n",ffname,line_nr,err_string); 
    }
}


const char kernel_file[]   = "panf3d.ptx";
/* Functions used */
const char noFluxFrontff[] = "noFluxFront";
const char noFluxBackff[]  = "noFluxBack";
const char noFluxNorthff[] = "noFluxNorth";
const char noFluxSouthff[] = "noFluxSouth";
const char solvPanf3dff[]  = "solvPanf3d";


int main(int argc, char *argv[]) {

    if(argc != 2) {
        fprintf(stderr,"wrong number of args\n"); exit(0);
    }

    DRIVER_ERROR(cuInit(0));

    const int max_iters = atoi(argv[1]);

    const size_t sizeof_arrays = (NX)*(NY+2U)*(NZ + 2U)*sizeof(double);
    double *e_cur,*e_prev,*r_cur;

    int deviceCount;
    DRIVER_ERROR(cuDeviceGetCount(&deviceCount));
    CUdevice device;
    DRIVER_ERROR(cuDeviceGet(&device,0));
    CUcontext contex;
    DRIVER_ERROR(cuCtxCreate(&contex,0,device));
    CUmodule module;
    DRIVER_ERROR(cuModuleLoad(&module,kernel_file));
    CUfunction solvPanf3d,noFluxFront,noFluxBack,noFluxSouth,noFluxNorth; 
    DRIVER_ERROR(cuModuleGetFunction(&solvPanf3d,module,solvPanf3dff));
    DRIVER_ERROR(cuModuleGetFunction(&noFluxFront,module,noFluxFrontff));
    DRIVER_ERROR(cuModuleGetFunction(&noFluxBack,module,noFluxBackff));
    DRIVER_ERROR(cuModuleGetFunction(&noFluxSouth,module,noFluxSouthff));
    DRIVER_ERROR(cuModuleGetFunction(&noFluxNorth,module,noFluxNorthff));

    DRIVER_ERROR(cuMemHostAlloc((void**)&e_cur,sizeof_arrays,CU_MEMHOSTALLOC_PORTABLE));    
    DRIVER_ERROR(cuMemHostAlloc((void**)&e_prev,sizeof_arrays,CU_MEMHOSTALLOC_PORTABLE));    
    DRIVER_ERROR(cuMemHostAlloc((void**)&r_cur,sizeof_arrays,CU_MEMHOSTALLOC_PORTABLE));    

    //init 
    for(unsigned int k = 1U; k <= NZ; k++) {
        for(unsigned int j = 1U; j <= NY; j++)  {
            for(unsigned int i = 0U; i < NX; i++) {
                const unsigned int index = k*(NX)*(NY+2U) + j*(NX) + i;
                e_prev[index] = ( i > NX/2U && k > NZ/2U ) ? 1.0 : 0.0;
	            r_cur[index] = ( j > NY/2U && k > NZ/2U ) ? 1.0 : 0.0;  
            }
        }
    }

 
    CUdeviceptr e_cur__device,e_prev__device,r_cur__device;
    DRIVER_ERROR(cuMemAlloc(&e_cur__device,sizeof_arrays));
    DRIVER_ERROR(cuMemAlloc(&e_prev__device,sizeof_arrays));
    DRIVER_ERROR(cuMemAlloc(&r_cur__device,sizeof_arrays));

    DRIVER_ERROR(cuMemcpyHtoD(e_cur__device, (const void *)e_cur,sizeof_arrays));
    DRIVER_ERROR(cuMemcpyHtoD(e_prev__device, (const void *)e_prev,sizeof_arrays));
    DRIVER_ERROR(cuMemcpyHtoD(r_cur__device, (const void *)r_cur,sizeof_arrays));
    
    int leastPriority,greatestPriority; 
    DRIVER_ERROR(cuCtxGetStreamPriorityRange(&leastPriority,&greatestPriority));

    int priFront  = 0;
    int priBack   = 0;
    int priNorth  = 0;
    int priSouth  = 0;
    int priSolver = 0;

    CUstream solvPanf3dStream;
    CUstream frontStream,backStream,southStream,northStream;
    //CU_STREAM_NON_BLOCKING -> don't sync with default 0 stream
    DRIVER_ERROR(cuStreamCreateWithPriority(&solvPanf3dStream,CU_STREAM_NON_BLOCKING,priSolver));
    DRIVER_ERROR(cuStreamCreateWithPriority(&frontStream,CU_STREAM_NON_BLOCKING,priFront));
    DRIVER_ERROR(cuStreamCreateWithPriority(&backStream,CU_STREAM_NON_BLOCKING,priBack));
    DRIVER_ERROR(cuStreamCreateWithPriority(&southStream,CU_STREAM_NON_BLOCKING,priSouth));
    DRIVER_ERROR(cuStreamCreateWithPriority(&northStream,CU_STREAM_NON_BLOCKING,priNorth));


    int offset_z = 0;
    int size_z   = 0;

    //Odd
    void *noFluxFrontArgs_o[] = {&e_prev__device};
    void *noFluxBackArgs_o[]  = {&e_prev__device};
    void *noFluxNorthArgs_o[] = {&e_prev__device,&offset_z};
    void *noFluxSouthArgs_o[] = {&e_prev__device,&offset_z};
    void *solvPanf3dArgs_o[]  = {&e_prev__device,&e_cur__device,&r_cur__device,
    &offset_z,&size_z}; 
    //Even 
    void *noFluxFrontArgs_e[] = {&e_cur__device};
    void *noFluxBackArgs_e[]  = {&e_cur__device};
    void *noFluxNorthArgs_e[] = {&e_cur__device,&offset_z};
    void *noFluxSouthArgs_e[] = {&e_cur__device,&offset_z};
    void *solvPanf3dArgs_e[]  = {&e_cur__device,&e_prev__device,&r_cur__device,
    &offset_z,&size_z}; 

    void *noFluxFrontArgs = noFluxFrontArgs_o;
    void *noFluxBackArgs = noFluxBackArgs_o;
    void *noFluxNorthArgs = noFluxNorthArgs_o;
    void *noFluxSouthArgs = noFluxSouthArgs_o;
    void *solvPanf3dArgs = solvPanf3dArgs_o; 
  

    static double time__omp = 0.0;
    time__omp -= omp_get_wtime();
  
    for(unsigned i = 0U; i < max_iters; i++) {
 
        DRIVER_ERROR(cuLaunchKernel(noFluxFront,
            THREAD_COUNT_X__BC,THREAD_COUNT_Y__BC,1,
            BLOCK_DIM_X__BC,BLOCK_DIM_Y__BC,1,
            0,
            frontStream,
            noFluxFrontArgs,NULL));
        
        DRIVER_ERROR(cuLaunchKernel(noFluxBack,
            THREAD_COUNT_X__BC,THREAD_COUNT_Y__BC,1,
            BLOCK_DIM_X__BC,BLOCK_DIM_Y__BC,1,
            0,
            backStream,
            noFluxBackArgs,NULL));

     
        DRIVER_ERROR(cuLaunchKernel(noFluxNorth,
            THREAD_COUNT_X__BC,THREAD_COUNT_Y__BC,1,
            BLOCK_DIM_X__BC,BLOCK_DIM_Y__BC,1,
            0,
            northStream,
            noFluxNorthArgs,NULL));
    
        DRIVER_ERROR(cuLaunchKernel(noFluxSouth,
            THREAD_COUNT_X__BC,THREAD_COUNT_Y__BC,1,
            BLOCK_DIM_X__BC,BLOCK_DIM_Y__BC,1,
            0,
            southStream,
            noFluxSouthArgs,NULL));

        DRIVER_ERROR(cuCtxSynchronize());
    
      
        DRIVER_ERROR(cuLaunchKernel(solvPanf3d,
            THREAD_COUNT_X__K,THREAD_COUNT_Y__K,THREAD_COUNT_Z__K,
            BLOCK_DIM_X__K,BLOCK_DIM_Y__K,BLOCK_DIM_Z__K,
            0,
            solvPanf3dStream,
            solvPanf3dArgs,NULL));        
       
        DRIVER_ERROR(cuCtxSynchronize());
    
        //Swap
        noFluxFrontArgs = (i&1U) ? noFluxFrontArgs_o : noFluxFrontArgs_e;
        noFluxBackArgs  = (i&1U) ? noFluxBackArgs_o  : noFluxBackArgs_e;
        noFluxNorthArgs = (i&1U) ? noFluxNorthArgs_o : noFluxNorthArgs_e;
        noFluxSouthArgs = (i&1U) ? noFluxSouthArgs_o : noFluxSouthArgs_e;    
        solvPanf3dArgs =  (i&1U) ? solvPanf3dArgs_o  : solvPanf3dArgs_e;
        
    }

    time__omp += omp_get_wtime();
    
    const double gflops = (max_iters * ((double)NX * (double)NY * (double)NZ) * 1e-9 * FLOPS)/time__omp;


    fprintf(stdout,"time: %f  GFLOPS: %f \n",time__omp,gflops);


    CUdeviceptr *e_res__device = (CUdeviceptr*) ((void **)solvPanf3dArgs)[0];

    DRIVER_ERROR(cuMemcpyDtoH(e_prev,*e_res__device,sizeof_arrays));

    double l2_e_prev = 0.f;

    // L2 norme 
    for(int k = 1; k < NZ +1; k++) {
        for(int j = 1; j < NY +1; j++) {
            for(int i = 0; i < NX; i++) {
                const int index = k*K_OFFSET + j*J_OFFSET + i;
                l2_e_prev += e_prev[index]*e_prev[index];
            }
        }
    }

    const double global_l2_norme = sqrt(l2_e_prev/(NX*NY*NZ));    
    fprintf(stdout,"l2:%f\n",global_l2_norme);


    DRIVER_ERROR(cuCtxDetach(contex));

    return 0;
}
