#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <stdlib.h> //exit

#include "cuda_util.h"
#include "panf3d_config.h"



extern "C" __global__ void noFluxFront(double * __restrict__ e_prev) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int j = 1 + threadIdx.y + blockIdx.y*blockDim.y;
   
        //printf("noFluxFront\n");
 
    const int index = 0*K_OFFSET+ j*J_OFFSET + i;
   #ifdef DEBUG 
    if((i < NX + 1) && (j < NY +1)) {
    #endif
        e_prev[index] = e_prev[index + 2*K_OFFSET];
    #ifdef DEBUF
    }
    #endif
}


extern "C" __global__ void noFluxBack(double * __restrict__ e_prev) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int j = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = (NZ + 1)*K_OFFSET+ j*J_OFFSET + i;
  #ifdef DEBUG  
    if((i < NX + 1) && (j < NY + 1)) {
    #endif
        e_prev[index] = e_prev[index - 2*K_OFFSET]; 
    #ifdef DEBUG
    }
    #endif
}

    

extern "C" __global__ void noFluxNorth(double * __restrict__ e_prev,const int offset_z) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET+ 0*J_OFFSET + i;
    #ifdef DEBUG
    if((i < NX + 1) && (k < NZ +1)) {
#endif
        e_prev[index] = e_prev[index + 2*J_OFFSET];
    #ifdef DEBUG
    }
    #endif
}

extern "C" __global__ void noFluxSouth(double * __restrict__ e_prev, const int offset_z) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET+ (NY +1)*J_OFFSET + i;
    #ifdef DEBUG
    if((i < NX + 1) && (k < NY + 1)) {
    #endif
        e_prev[index] = e_prev[index - 2*J_OFFSET];
    #ifdef DEBUG
    }
    #endif
}
//DEP
extern "C" __global__ void noFluxWest(double * __restrict__ e_prev) {

    const int j = 1 + threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET+ j*J_OFFSET + (NX +1);
  #ifdef DEBUG
    if((j < NY + 1) && (k < NZ +1)) {
    #endif
        e_prev[index] = e_prev[index - 2];
    #ifdef DEBUG
    }
    #endif
}
//DEP
extern "C" __global__ void noFluxEast(double * __restrict__ e_prev) {

    const int j = 1 + threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET+ j*J_OFFSET  + 0;
   #ifdef DEBUG
    if((j < NY + 1) && (k < NZ +1)) {
     #endif
        e_prev[index] = e_prev[index + 2];
    #ifdef DEBUG
    }
    #endif
}



extern "C" __global__ void solvPanf3d( double const *__restrict__ const e_prv,double *__restrict__  e_cur, double * __restrict__ r_cur, 
    const int global_offset_z, const int size_z) {

    __shared__ double sharedMem[BLOCK_DIM_X__K+1][BLOCK_DIM_Y__K+1];
    
    //printf("solvPanf3d\n");    

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int j = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    const int k = 1 + blockIdx.z*BLOCK_DIM_Z_FOR;

    int index = k*K_OFFSET + j*J_OFFSET + i;
   
    register double e_left;
    register double e_right;

    #ifdef DEBGU
    if((i < NX +1)  && (j < NY +1) ) {
    #endif
        register double center =  e_prv[index];
        register double north = e_prv[index + K_OFFSET];
        register double south = e_prv[index - K_OFFSET];
         sharedMem[threadIdx.x+1][threadIdx.y+1] = center; 
        if(threadIdx.x == 0 || threadIdx.x == blockDim.x) {
            const register int shared_index = index + ((threadIdx.x == 0)? -1: -1)*K_OFFSET;
            sharedMem[threadIdx.x][threadIdx.y] = e_prv[shared_index];
        } 
        if(threadIdx.y == 0 || threadIdx.y == blockDim.y) {
            const register int shared_index = index + ((threadIdx.y == 0)? -1: -1)*J_OFFSET;
            sharedMem[threadIdx.x][threadIdx.y] = e_prv[shared_index];
        }  

 
 if(i == 0)  {

            e_left = e_prv[index +1];



    } else {
        e_left = e_prv[index -1];

    }
    if(i == (NX -1)) {
        e_right = e_prv[index -1];
    } else {
        e_right = e_prv[index + 1];

    }    

    
 
        e_cur[index] = center + ALPHA*(e_left + e_right - 6.0*center + e_prv[index + J_OFFSET] + e_prv[index - J_OFFSET] + north + south);

        e_cur[index] += -DT*(KK*e_cur[index]*(e_cur[index]-A)*(e_cur[index] - 1.0) +e_cur[index]*r_cur[index]);
        r_cur[index] += DT*(EPSILON + M1*r_cur[index]/(e_cur[index] + M2))*(-r_cur[index] - KK*e_cur[index]*(e_cur[index] - B-1.0));         
    
    
    
    #pragma unroll 
    for(int _k = 1 ; _k< BLOCK_DIM_Z_FOR; _k++) {
        #ifdef DEBUG
        if((_k + k ) < (NZ +1)) {
     #endif
            index += K_OFFSET;
            south = center;
            center = north;
            north = e_prv[index +K_OFFSET];
    
    if(i == 0)  {
        
            e_left = e_prv[index +1];


    } else {
        e_left = e_prv[index -1];

    }
    if(i == (NX-1)) {
        //printf("blockId.x:%d Threadcount:%d threadIdx.x:%d  blockDim.x:%d\n",blockIdx.x,THREAD_COUNT_X__K,threadIdx.x,blockDim.x);
        e_right = e_prv[index-1];
    } else {
        e_right = e_prv[index + 1];

    }    


 
            e_cur[index] = center + ALPHA*(e_left + e_right - 6.0*center + e_prv[index + J_OFFSET] + e_prv[index - J_OFFSET] + north + south);

            e_cur[index] += -DT*(KK*e_cur[index]*(e_cur[index]-A)*(e_cur[index]-1.0) + e_cur[index]*r_cur[index]);
            r_cur[index] += DT*(EPSILON + M1*r_cur[index]/(e_cur[index] + M2))*(-r_cur[index]-KK*e_cur[index]*(e_cur[index] - B-1.0));     
       #ifdef DEBUG
         }
        #endif
    }
   #ifdef DEBUG

    }
    #endif
    
}


