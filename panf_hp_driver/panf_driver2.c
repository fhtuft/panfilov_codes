#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <stdlib.h> //exit

#include <cuda.h> //driver api
#include <builtin_types.h>


int main(int argc, char *argv[]) {

    if(argc != 2) {
        fprintf(stderr,"wrong number of args\n"); exit(0);
    }
       
           
    const int max_iters = atoi(argv[1]);

    const size_t sizeof_arrays = (NX)*(NY+2U)*(NZ + 2U)*sizeof(double);
    double *e_cur,*e_prev,*r_cur;
    CUDA_ERROR(cudaHostAlloc((void**)&e_cur,sizeof_arrays,cudaHostAllocDefault),
    "cudaHostAlloc");
    CUDA_ERROR(cudaHostAlloc((void**)&e_prev,sizeof_arrays,cudaHostAllocDefault),
    "cudaHostAlloc");
    CUDA_ERROR(cudaHostAlloc((void**)&r_cur,sizeof_arrays,cudaHostAllocDefault),
    "cudaHostAlloc");
    
    for(unsigned int k = 1U; k <= NZ; k++) {
        for(unsigned int j = 1U; j <= NY; j++)  {
            for(unsigned int i = 0U; i < NX; i++) {
                const unsigned int index = k*(NX)*(NY+2U) + j*(NX) + i;
                e_prev[index] = ( i > NX/2U && k > NZ/2U ) ? 1.0 : 0.0;
	            r_cur[index] = ( j > NY/2U && k > NZ/2U ) ? 1.0 : 0.0;  
            }
        }
    }

    double *e_cur__device;
    double *e_prev__device;
    double *r_cur__device;

    CUDA_ERROR(cudaMalloc((void**)&e_cur__device,sizeof_arrays),
    "cudaMalloc");
    CUDA_ERROR(cudaMalloc((void**)&e_prev__device,sizeof_arrays),
    "cudaMalloc");
    CUDA_ERROR(cudaMalloc((void**)&r_cur__device,sizeof_arrays),
    "cudaMalloc");
   
    CUDA_ERROR(cudaMemcpy(e_cur__device,e_cur,sizeof_arrays,cudaMemcpyHostToDevice),"cudaMamcpy"); 
    CUDA_ERROR(cudaMemcpy(e_prev__device,e_prev,sizeof_arrays,cudaMemcpyHostToDevice),"cudaMemcpy"); 
    CUDA_ERROR(cudaMemcpy(r_cur__device,r_cur,sizeof_arrays,cudaMemcpyHostToDevice),"cudaMemcpy"); 

    const dim3 threadBlocks__Kernel(THREAD_COUNT_X__K,THREAD_COUNT_Y__K,THREAD_COUNT_Z__K);
    const dim3 threadsPerBlock__Kernel(BLOCK_DIM_X__K,BLOCK_DIM_Y__K,BLOCK_DIM_Z__K);
    const dim3 threadBlocks__BC(THREAD_COUNT_X__BC,THREAD_COUNT_Y__BC,1);
    const dim3 threadsPerBlock__BC(BLOCK_DIM_X__BC,BLOCK_DIM_Y__BC,1);
 
    /* Stream */
    cudaStream_t compute_stream;
    cudaStream_t north_stream,south_stream,west_stream,east_stream,front_stream,back_stream;
    CUDA_ERROR(cudaStreamCreate(&compute_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&north_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&south_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&west_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&east_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&front_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&back_stream),"cudaStreamCreate");
    
    static double time__omp = 0.0;
    time__omp -= omp_get_wtime();

    CUDA_ERROR(cudaDeviceSetCacheConfig(cudaFuncCachePreferL1),"cudaDeviceSetCachConfig");
 
    for(unsigned int i = 0U; i < max_iters; i++) {
        
        noFluxFront<<<threadBlocks__BC,threadsPerBlock__BC,0,front_stream>>>(e_prev__device);
        noFluxBack<<<threadBlocks__BC,threadsPerBlock__BC,0,back_stream>>>(e_prev__device);
        noFluxNorth<<<threadBlocks__BC,threadsPerBlock__BC,0,north_stream>>>(e_prev__device);
        noFluxSouth<<<threadBlocks__BC,threadsPerBlock__BC,0,south_stream>>>(e_prev__device);
        //noFluxWest<<<threadBlocks__BC,threadsPerBlock__BC,0,west_stream>>>(e_prev__device);
        //noFluxEast<<<threadBlocks__BC,threadsPerBlock__BC,0,east_stream>>>(e_prev__device);

     CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSyncronice");


        solvPanf3d<<<threadBlocks__Kernel,threadsPerBlock__Kernel,0,compute_stream>>>(e_prev__device,e_cur__device,r_cur__device);
 
    
     CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSyncronice");

 
    //swap
    {
        register double *tmp__e = e_cur__device;
        e_cur__device = e_prev__device;
        e_prev__device = tmp__e;
    }
    
    }
    time__omp += omp_get_wtime();
    
    const double gflops = (max_iters * ((double)NX * (double)NY * (double)NZ) * 1e-9 * FLOPS)/time__omp;

    CUDA_ERROR(cudaMemcpy(e_prev,e_prev__device,sizeof_arrays,cudaMemcpyDeviceToHost),"cudaMemcpy");

    double l2_e_prev = 0.f;
#if 1 
    for(int k = 1; k < NZ +1; k++) {
        for(int j = 1; j < NY +1; j++) {
            for(int i = 0; i < NX; i++) {
                const int index = k*K_OFFSET + j*J_OFFSET + i;
                l2_e_prev += e_prev[index]*e_prev[index];
            }
        }
    }
    printf("l2_e_prev :%f \n",l2_e_prev);
#endif
    const double global_l2_norme = sqrt(l2_e_prev/(NX*NY*NZ));    


    fprintf(stdout,"time: %f GFLOPS: %f, l2:%f\n",time__omp,gflops,global_l2_norme);
    

    CUDA_ERROR(cudaFreeHost(e_cur),"cudaFreeHost");
    CUDA_ERROR(cudaFreeHost(e_prev),"cudaFreeHost");
    CUDA_ERROR(cudaFreeHost(r_cur),"cudaFreeHost");
    
    CUDA_ERROR(cudaFree(e_cur__device),"cudaFree");
    CUDA_ERROR(cudaFree(e_prev__device),"cudaFree");
    CUDA_ERROR(cudaFree(r_cur__device),"cudaFree");
    
    CUDA_ERROR(cudaStreamDestroy(compute_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(north_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(south_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(west_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(east_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(front_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(back_stream),"cudadStreamDestroy");

    CUDA_ERROR(cudaDeviceReset(),"cudaDeviceReset");

    exit(0);
    
}

