#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <stdlib.h> //exit

#include "cuda_util.h"
#include "panf3d_config.h"



__global__ void noFluxFront(double * __restrict__ e_prev) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int j = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = 0*K_OFFSET+ j*J_OFFSET + i;
   #ifdef DEBUG 
    if((i < NX + 1) && (j < NY +1)) {
    #endif
        e_prev[index] = e_prev[index + 2*K_OFFSET];
    #ifdef DEBUF
    }
    #endif
}


__global__ void noFluxBack(double * __restrict__ e_prev) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int j = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = (NZ + 1)*K_OFFSET+ j*J_OFFSET + i;
  #ifdef DEBUG  
    if((i < NX + 1) && (j < NY + 1)) {
    #endif
        e_prev[index] = e_prev[index - 2*K_OFFSET]; 
    #ifdef DEBUG
    }
    #endif 
}

    

__global__ void noFluxNorth(double * __restrict__ e_prev) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET+ 0*J_OFFSET + i;
    #ifdef DEBUG
    if((i < NX + 1) && (k < NZ +1)) {
#endif
        e_prev[index] = e_prev[index + 2*J_OFFSET];
    #ifdef DEBUG
    }
    #endif
}

__global__ void noFluxSouth(double * __restrict__ e_prev) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET + (NY +1)*J_OFFSET + i;
    #ifdef DEBUG
    if((i < NX + 1) && (k < NY + 1)) {
    #endif
        e_prev[index] = e_prev[index - 2*J_OFFSET];
    #ifdef DEBUG
    }
    #endif
}
//DEP
__global__ void noFluxWest(double * __restrict__ e_prev) {

    const int j = 1 + threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET+ j*J_OFFSET + (NX +1);
  #ifdef DEBUG
    if((j < NY + 1) && (k < NZ +1)) {
    #endif
        e_prev[index] = e_prev[index - 2];
    #ifdef DEBUG
    }
    #endif
}
//DEP
__global__ void noFluxEast(double * __restrict__ e_prev) {

    const int j = 1 + threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET+ j*J_OFFSET  + 0;
   #ifdef DEBUG
    if((j < NY + 1) && (k < NZ +1)) {
     #endif
        e_prev[index] = e_prev[index + 2];
    #ifdef DEBUG
    }
    #endif
}



__global__ void solvPanf3d( double const *__restrict__ const e_prv,double *__restrict__  e_cur, double * __restrict__ r_cur) {

    __shared__ double sharedMem[BLOCK_DIM_X__K+1][BLOCK_DIM_Y__K+1];
    
    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int j = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    const int k = 1 + blockIdx.z*BLOCK_DIM_Z_FOR;

    int index = k*K_OFFSET + j*J_OFFSET + i;
   
    register double e_left;
    register double e_right;

    #ifdef DEBGU
    if((i < NX +1)  && (j < NY +1) ) {
    #endif
        register double center =  e_prv[index];
        register double north = e_prv[index + K_OFFSET];
        register double south = e_prv[index - K_OFFSET];
         sharedMem[threadIdx.x+1][threadIdx.y+1] = center; 
        if(threadIdx.x == 0 || threadIdx.x == blockDim.x) {
            const register int shared_index = index + ((threadIdx.x == 0)? -1: -1)*K_OFFSET; //TODO ?? -1
            sharedMem[threadIdx.x][threadIdx.y] = e_prv[shared_index];
        } 
        if(threadIdx.y == 0 || threadIdx.y == blockDim.y) {
            const register int shared_index = index + ((threadIdx.y == 0)? -1: -1)*J_OFFSET;
            sharedMem[threadIdx.x][threadIdx.y] = e_prv[shared_index];
        }  

 
 if(i == 0)  {

            e_left = e_prv[index +1];



    } else {
        e_left = e_prv[index -1];

    }
    if(i == (NX -1)) {
        e_right = e_prv[index -1];
    } else {
        e_right = e_prv[index + 1];

    }    

    
 
        e_cur[index] = center + ALPHA*(e_left + e_right - 6.0*center + e_prv[index + J_OFFSET] + e_prv[index - J_OFFSET] + north + south);

        e_cur[index] += -DT*(KK*e_cur[index]*(e_cur[index]-A)*(e_cur[index] - 1.0) +e_cur[index]*r_cur[index]);
        r_cur[index] += DT*(EPSILON + M1*r_cur[index]/(e_cur[index] + M2))*(-r_cur[index] - KK*e_cur[index]*(e_cur[index] - B-1.0));         
    
    
    
    #pragma unroll 
    for(int __k = 1 ; __k< BLOCK_DIM_Z_FOR; __k++) {
        #ifdef DEBUG
        if((__k + k ) < (NZ +1)) {
     #endif
            index += K_OFFSET;
            south = center;
            center = north;
            north = e_prv[index +K_OFFSET];
    
    if(i == 0)  {
        
            e_left = e_prv[index +1];


    } else {
        e_left = e_prv[index -1];

    }
    if(i == (NX-1)) {
        //printf("blockId.x:%d Threadcount:%d threadIdx.x:%d  blockDim.x:%d\n",blockIdx.x,THREAD_COUNT_X__K,threadIdx.x,blockDim.x);
        e_right = e_prv[index-1];
    } else {
        e_right = e_prv[index + 1];

    }    


 
            e_cur[index] = center + ALPHA*(e_left + e_right - 6.0*center + e_prv[index + J_OFFSET] + e_prv[index - J_OFFSET] + north + south);

            e_cur[index] += -DT*(KK*e_cur[index]*(e_cur[index]-A)*(e_cur[index]-1.0) + e_cur[index]*r_cur[index]);
            r_cur[index] += DT*(EPSILON + M1*r_cur[index]/(e_cur[index] + M2))*(-r_cur[index]-KK*e_cur[index]*(e_cur[index] - B-1.0));     
       #ifdef DEBUG
         }
        #endif
    }
   #ifdef DEBUG

    }
    #endif
    
}

int main(int argc, char *argv[]) {

    if(argc != 2) {
        fprintf(stderr,"wrong number of args\n"); exit(0);
    }
       
           
    const int max_iters = atoi(argv[1]);

    const size_t sizeof_arrays = (NX)*(NY+2U)*(NZ + 2U)*sizeof(double);
    double *e_cur,*e_prev,*r_cur;
    CUDA_ERROR(cudaHostAlloc((void**)&e_cur,sizeof_arrays,cudaHostAllocDefault),
    "cudaHostAlloc");
    CUDA_ERROR(cudaHostAlloc((void**)&e_prev,sizeof_arrays,cudaHostAllocDefault),
    "cudaHostAlloc");
    CUDA_ERROR(cudaHostAlloc((void**)&r_cur,sizeof_arrays,cudaHostAllocDefault),
    "cudaHostAlloc");
    
    for(unsigned int k = 1U; k <= NZ; k++) {
        for(unsigned int j = 1U; j <= NY; j++)  {
            for(unsigned int i = 0U; i < NX; i++) {
                const unsigned int index = k*(NX)*(NY+2U) + j*(NX) + i;
                e_prev[index] = ( i > NX/2U && k > NZ/2U ) ? 1.0 : 0.0;
	            r_cur[index] = ( j > NY/2U && k > NZ/2U ) ? 1.0 : 0.0;  
            }
        }
    }

    double *e_cur__device;
    double *e_prev__device;
    double *r_cur__device;

    CUDA_ERROR(cudaMalloc((void**)&e_cur__device,sizeof_arrays),
    "cudaMalloc");
    CUDA_ERROR(cudaMalloc((void**)&e_prev__device,sizeof_arrays),
    "cudaMalloc");
    CUDA_ERROR(cudaMalloc((void**)&r_cur__device,sizeof_arrays),
    "cudaMalloc");
   
    CUDA_ERROR(cudaMemcpy(e_cur__device,e_cur,sizeof_arrays,cudaMemcpyHostToDevice),"cudaMamcpy"); 
    CUDA_ERROR(cudaMemcpy(e_prev__device,e_prev,sizeof_arrays,cudaMemcpyHostToDevice),"cudaMemcpy"); 
    CUDA_ERROR(cudaMemcpy(r_cur__device,r_cur,sizeof_arrays,cudaMemcpyHostToDevice),"cudaMemcpy"); 

    const dim3 threadBlocks__Kernel(THREAD_COUNT_X__K,THREAD_COUNT_Y__K,THREAD_COUNT_Z__K);
    const dim3 threadsPerBlock__Kernel(BLOCK_DIM_X__K,BLOCK_DIM_Y__K,BLOCK_DIM_Z__K);
    const dim3 threadBlocks__BC(THREAD_COUNT_X__BC,THREAD_COUNT_Y__BC,1);
    const dim3 threadsPerBlock__BC(BLOCK_DIM_X__BC,BLOCK_DIM_Y__BC,1);
 
    /* Stream */
    cudaStream_t compute_stream;
    cudaStream_t north_stream,south_stream,west_stream,east_stream,front_stream,back_stream;
    CUDA_ERROR(cudaStreamCreate(&compute_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&north_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&south_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&west_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&east_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&front_stream),"cudaStreamCreate");
    CUDA_ERROR(cudaStreamCreate(&back_stream),"cudaStreamCreate");
    
    static double time__omp = 0.0;
    time__omp -= omp_get_wtime();

    CUDA_ERROR(cudaDeviceSetCacheConfig(cudaFuncCachePreferL1),"cudaDeviceSetCachConfig");
 
    for(unsigned int i = 0U; i < max_iters; i++) {
        
        noFluxFront<<<threadBlocks__BC,threadsPerBlock__BC,0,front_stream>>>(e_prev__device);
        noFluxBack<<<threadBlocks__BC,threadsPerBlock__BC,0,back_stream>>>(e_prev__device);
        noFluxNorth<<<threadBlocks__BC,threadsPerBlock__BC,0,north_stream>>>(e_prev__device);
        noFluxSouth<<<threadBlocks__BC,threadsPerBlock__BC,0,south_stream>>>(e_prev__device);
        //noFluxWest<<<threadBlocks__BC,threadsPerBlock__BC,0,west_stream>>>(e_prev__device);
        //noFluxEast<<<threadBlocks__BC,threadsPerBlock__BC,0,east_stream>>>(e_prev__device);

     CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSyncronice");


        solvPanf3d<<<threadBlocks__Kernel,threadsPerBlock__Kernel,0,compute_stream>>>(e_prev__device,e_cur__device,r_cur__device);
 
    
     CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSyncronice");

 
    //swap
    {
        register double *tmp__e = e_cur__device;
        e_cur__device = e_prev__device;
        e_prev__device = tmp__e;
    }
    
    }
    time__omp += omp_get_wtime();
    
    const double gflops = (max_iters * ((double)NX * (double)NY * (double)NZ) * 1e-9 * FLOPS)/time__omp;

    CUDA_ERROR(cudaMemcpy(e_prev,e_prev__device,sizeof_arrays,cudaMemcpyDeviceToHost),"cudaMemcpy");

    double l2_e_prev = 0.f;
#if 1 
    for(int k = 1; k < NZ +1; k++) {
        for(int j = 1; j < NY +1; j++) {
            for(int i = 0; i < NX; i++) {
                const int index = k*K_OFFSET + j*J_OFFSET + i;
                l2_e_prev += e_prev[index]*e_prev[index];
            }
        }
    }
    printf("l2_e_prev :%f \n",l2_e_prev);
#endif
    const double global_l2_norme = sqrt(l2_e_prev/(NX*NY*NZ));    


    fprintf(stdout,"time: %f GFLOPS: %f, l2:%f\n",time__omp,gflops,global_l2_norme);
    

    CUDA_ERROR(cudaFreeHost(e_cur),"cudaFreeHost");
    CUDA_ERROR(cudaFreeHost(e_prev),"cudaFreeHost");
    CUDA_ERROR(cudaFreeHost(r_cur),"cudaFreeHost");
    
    CUDA_ERROR(cudaFree(e_cur__device),"cudaFree");
    CUDA_ERROR(cudaFree(e_prev__device),"cudaFree");
    CUDA_ERROR(cudaFree(r_cur__device),"cudaFree");
    
    CUDA_ERROR(cudaStreamDestroy(compute_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(north_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(south_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(west_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(east_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(front_stream),"cudadStreamDestroy");
    CUDA_ERROR(cudaStreamDestroy(back_stream),"cudadStreamDestroy");

    CUDA_ERROR(cudaDeviceReset(),"cudaDeviceReset");

    exit(0);
    
}

