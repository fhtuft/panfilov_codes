#ifndef PANF_CONFIG_H
#define PANF_CONFIG_H
#include <assert.h>

#define FLOPS 30  /* Number of FLOPS panfilov3D */

/*Card spesifics */
#define MAXTHREADS_BLOCK  1024
#define MEMORY_SIZE_CUDA   

#define  NX  512  /* X dimension of the grid */  
#define  NY  512  /* Y dimension of the grid */
#define  NZ  512  /* Z dimension of the grid */
//STATIC_ASSERT(NX%16 == 0);
//static_assert(NY%16 == 0,"");
//_Static_assert(NZ%16 == 0,"");

#define I_OFFSET 0
#define J_OFFSET (NX)
#define K_OFFSET ((NX)*(NY+2))


#define BLOCK_DIM_X__K  64  /* used for kernel launch */
#define BLOCK_DIM_Y__K   8  /* used for kernel launch */
#define BLOCK_DIM_Z__K   1  /* used for kernel launch*/
#define BLOCK_DIM_Z_FOR  8  /* used in for loop inside kernel*/
//static_assert(BLOCK_DIM_X__K*BLOCK_DIM_Y__K*BLOCK_DIM_Z__K <= MAXTHREADS_BLOCK,"");

#define THREAD_COUNT_X__K  (NX/BLOCK_DIM_X__K)
#define THREAD_COUNT_Y__K  (NY/BLOCK_DIM_Y__K)
#define THREAD_COUNT_Z__K  (NZ/(BLOCK_DIM_Z__K*BLOCK_DIM_Z_FOR))
//static_assert(NX%BLOCK_DIM_X__K == 0,"");
//static_assert(NY%BLOCK_DIM_Y__K == 0,"");
//static_assert(NZ%(BLOCK_DIM_Z__K*BLOCK_DIM_Z_FOR) == 0,"");

#define BLOCK_DIM_X__BC  32  /* used for kernel launch */
#define BLOCK_DIM_Y__BC  32  /* used for kernel launch */
#define BLOCK_DIM_Z__BC   1  /* used for kernel launch  */
//static_assert(BLOCK_DIM_X__BC*BLOCK_DIM_Y__BC*BLOCK_DIM_Z__BC <= MAXTHREADS_BLOCK,"");

#define THREAD_COUNT_X__BC  (NX/BLOCK_DIM_X__BC)
#define THREAD_COUNT_Y__BC  (NY/BLOCK_DIM_Y__BC)
#define THREAD_COUNT_Z__BC  (NZ/BLOCK_DIM_Z__BC)
//static_assert(NX%BLOCK_DIM_X__BC == 0,"");
//static_assert(NY%BLOCK_DIM_Y__BC == 0,"");
//static_assert(NZ%BLOCK_DIM_Z__BC == 0,"");



#define A       0.1
#define KK      8.0
#define EPSILON 0.01
#define M1      0.07  
#define M2      0.3
#define B       0.1
#define D       5e-5
#define H       (1.0/(NX - 1))
#define RP      (KK*(B + 1.0)*(B + 1.0)/6.0)
#define DTE     ((H*H)/(D*6.0 + (H*H)*(RP+KK)))
#define DTR     (1.0/(EPSILON + ((M1/M2)*RP)))
//#define DT      DTR*0.95
#define DT      ((DTE<DTR) ? 0.95*DTE : 0.95*DTR)
#define ALPHA   (D*DT/(H*H))
const double  dt = DT;
const double  alpha = ALPHA; 




#endif
