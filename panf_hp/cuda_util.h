/* 
 Util for cuda stuff
*/
#ifndef CUDA_UTIL_H
#define CUDA_UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>



/*- CUDA error handling -*/
#define CUDA_ERROR(ERROR_CODE,FUNCTION) __cuda_error((ERROR_CODE),(FUNCTION),__LINE__,__FILE__)
static inline void __cuda_error(cudaError_t error,const char *function,const int line,const char *filename) {

  if(error != cudaSuccess) {
    fprintf(stderr,"CUDA ERROR: %s ,function: %s line: %d  %s \n",
	    cudaGetErrorString(error),function,line,filename);
    exit(EXIT_FAILURE);
  }
    
}


#endif
